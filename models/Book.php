<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Book extends ActiveRecord
{
	public function accessRules()
	{
	    return array(
	        array('allow',  // позволим всем пользователям выполнять действия 'list' и 'show'
	            'actions'=>array('index', 'view'),
	            'users'=>array('*'),
	        ),
	        array('allow', // позволим аутентифицированным пользователям выполнять любые действия
	            'users'=>array('@'),
	        ),
	        array('deny',  // остальным запретим всё
	            'users'=>array('*'),
	        ),
	    );
	}
	public function rules()
    {
        return array(
            ['name', 'required', 'message' => 'Название не может быть пустоым'],
            ['date', 'required', 'message' => 'Дата выхода книги не может быть пустоым'],
            ['author_id', 'required', 'message' => 'Автор не может быть пустоым'],
            ['preview','safe'],
            ['description','safe'],
        );
    } 

    public function getAuthor()
    {
    	return $this->hasOne(Author::className(), ['id' => 'author_id']);
        // return ['author'=>array(self::BELONGS_TO, 'Author', 'author_id')];
    }

    public function beforeSave($insert)
	{
        if($this->isNewRecord)
        {
            $this->date_create= date('Y.m.d H:i:s', time());
        	$this->date_update = date('Y.m.d H:i:s', time());
        	$this->date = date('Y.m.d H:i:s', strtotime($this->date));
        }
        else
        {
            $this->date_update = date('Y.m.d H:i:s', time());
        	$this->date = date('Y.m.d H:i:s', strtotime($this->date));
	    }
	    return parent::beforeSave($insert);
	    
	}
}
 ?>