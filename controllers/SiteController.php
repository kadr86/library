<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Author;
use app\models\Book;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'show'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        
        if (isset($_REQUEST['Book']) && !empty($_REQUEST['Book']))
        {
            $book = $_REQUEST['Book'];
            $attrs = [];
            $date = '';
            if (!empty($book['author']))
            {
                $attrs['author_id'] = $book['author'];
            }
            if (!empty($book['name']))
            {
                $attrs['name'] = $book['name'];
            }
            if (!empty($book['date_from']))
            {
                if (empty($book['date_to'])) $book['date_to'] = date('Y.m.d', time());
                $date = ['between', 'date', date('Y-m-d', strtotime($book['date_from'])), date('Y-m-d', strtotime($book['date_to']))];
            }
            
            $books = Book::find()->with('author')->where($attrs)->andWhere($date)->orderBy('date_update DESC')->all();
        }
        else
            $books = Book::find()->with('author')->orderBy('date_update DESC')->all();

        $query = Author::find();
        $authors_db = $query->all();
        $authors = [];
        foreach ($authors_db as $key => $author) 
        {
            $authors[$author->id] = $author->firstname.' '.$author->lastname;
        }
        return $this->render('index', [
                'books' => $books,
                'authors' => $authors,
            ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionCreate()
    {

        if (isset($_REQUEST['Book']))
        {
            $uploads_files = array();
            $uploads_dir = $_SERVER['DOCUMENT_ROOT'].'web/images';
            $files = $_FILES['Book'];
            $path = '';
            if (!empty($files)) 
            {
                
                $tmp_name = $files["tmp_name"]['preview'];
                $name = $files["name"]['preview'];
                move_uploaded_file($tmp_name, "$uploads_dir/$name");
                $path = "$uploads_dir/$name";
            }
            $path = str_replace($_SERVER['DOCUMENT_ROOT'], '/', $path);
            $_REQUEST['Book']['preview'] = $path;
        }
        $model = new Book();
        if ($model->load($_REQUEST) && $model->save()) {
                return Yii::$app->response->redirect(['site/index']);
        } 
        else 
        {
            $query = Author::find();
            $authors_db = $query->all();
            $authors = [];
            foreach ($authors_db as $key => $author) 
            {
                $authors[$author->id] = $author->firstname.' '.$author->lastname;
            }
            return $this->render('create', ['model' => $model, 'authors' => $authors]);
        }
    }

    public function actionUpdate()
    {

        if (!empty($_FILES['Book']['name']['preview']))
        {
            $uploads_files = array();
            $uploads_dir = $_SERVER['DOCUMENT_ROOT'].'web/images';
            $files = $_FILES['Book'];
            $path = '';
            if (!empty($files)) 
            {
                
                $tmp_name = $files["tmp_name"]['preview'];
                $name = $files["name"]['preview'];
                move_uploaded_file($tmp_name, "$uploads_dir/$name");
                $path = "$uploads_dir/$name";
            }
            $path = str_replace($_SERVER['DOCUMENT_ROOT'], '/', $path);
            $_REQUEST['Book']['preview'] = $path;
        }
        elseif(isset($_REQUEST['Book']))
        {
            $_REQUEST['Book']['preview'] = $_REQUEST['Book']['preview_cache'];
        }
        $id = $_REQUEST['id'];
        $model = Book::find()->with('author')->one();
        if ($model->load($_REQUEST) && $model->save()) {
                return Yii::$app->response->redirect(['site/index']);
        } 
        else 
        {
            $query = Author::find();
            $authors_db = $query->all();
            $authors = [];
            foreach ($authors_db as $key => $author) 
            {
                $authors[$author->id] = $author->firstname.' '.$author->lastname;
            }
            
            return $this->render('update', ['model' => $model, 'authors' => $authors]);
        }
    }

    public function actionDelete() 
    {
        $id = $_REQUEST['id'];
        Book::find()->where(['id' => $id])->one()->delete();
        return $this->redirect(['site/index']);
    }

    public function actionShow()
    {
        $id = $_REQUEST['id'];
        $model = Book::find()->where(['id' => $id])->one();
        return $this->render('show', ['model' => $model]);
    }
}
