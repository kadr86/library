<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm; 


$this->title = 'Библиотека';?>

<div class="site-index">
<form class='ui form' method="get">
	<div class="four wide fields">
		<div class="field">
			<select name="Book[author]">
				<?php foreach ($authors as $key => $author):?>
					<option value="<?php echo $key ?>"><?php echo $author ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="field">
			<input type="text" name="Book[name]" placeholder ='Название' value="<?php echo !empty($_REQUEST['Book']['name']) ? $_REQUEST['Book']['name'] : '' ?>">
		</div>
	</div>
	<div class="two wide fields">
		<div class="four wide fields">
			<div class="field">
				<label  style="padding-top: 7px;">Дата выхода книги</label>
			</div>
			<div class="field">
				<input type="text" name="Book[date_from]" value="<?php echo !empty($_REQUEST['Book']['date_from']) ? $_REQUEST['Book']['date_from'] : '' ?>">
			</div>
			<div class="field" align="center">
				<label  style="padding-top: 7px;">по</label>
			</div>
			<div class="field">
				<input type="text" name="Book[date_to]" value="<?php echo !empty($_REQUEST['Book']['date_to']) ? $_REQUEST['Book']['date_to'] : '' ?>">
			</div>
		</div>
	</div>
	<div class="field" align="right">
		<input type="submit" value='Искать' class="ui primary button">
	</div>
</form>
<table class="ui celled structured table">
  <thead>
    <tr>
      <th rowspan="2">Id</th>
      <th rowspan="2">Название</th>
      <th rowspan="2">Превью</th>
      <th rowspan="2">Автор</th>
      <th rowspan="2">Дата выхода книги</th>
      <th rowspan="2">Дата обновления</th>
      <th colspan="3" rowspan="2">Кнопки действия</th>
    </tr>
    
  </thead>
  <tbody>
  	<?php foreach ($books as $key => $book):?>
	    <tr>
	      <td><?php echo $book->id ?></td>
	      <td><?php echo $book->name ?></td>
	      <td>
	      		<a href="#" id='preview_<?php echo $book->id ?>'><img class="preview" src="<?php echo $book->preview ?>" alt="" width='100px'></a>
	      		<div id="block_preview_<?php echo $book->id ?>" class="big_prev">
    				<img src="<?php echo $book->preview ?>" alt="" width='500px'>
    			</div>
	      </td>

	      <td><?php echo $book->author->firstname.' '.$book->author->lastname ?></td>
	      <td><?php echo date('d.m.Y', strtotime($book->date)) ?></td>
	      <td><?php echo date('d.m.Y H:i', strtotime($book->date_update)) ?></td>
	      <td class="right aligned"><?php echo Html::a('ред', ['site/update', 'id' => $book->id]);?></td>
	      <td class="center aligned"><?php echo Html::a('просмотр', ['site/show', 'id' => $book->id]);?></td>
	      <td><?php echo Html::a('Удалить', ['site/delete', 'id' => $book->id], ['onclick' => 'confirm("Удалить?");']);?></td>
	      
	    </tr>
	<?php endforeach ?>
  </tbody>
</table>
    
</div>
