
<?php 
	use yii\bootstrap\ActiveForm; 
	use yii\helpers\Html;

?>
<div class="create book">
	<?php $form = ActiveForm::begin(array('options' => array('class' => 'ui form', 'enctype' => 'multipart/form-data')));
		echo '<div class="field">';
			echo $form->field($model, 'name')->textInput(['placeholder' => 'Название'])->label('Название') ;
		echo '</div>';
		echo '<div class="field">';?>
			<img src="<?php echo $model->preview ?>" width='250px' alt="">
			<input type="hidden" value="<?php echo $model->preview ?>" name='Book[preview_cache]'> 
			<?
			echo $form->field($model, 'preview')->fileInput()->label('Обложка');
		echo '</div>';
		echo '<div class="three wide fields">';
			echo '<div class="field">';
				echo $form->field($model, 'date',['inputOptions' => ['value' => date( 'd.m.Y', strtotime($model->date) )]])->textInput(['placeholder' => 'Дата выпуска'])->label('Дата выпуска');
			echo '</div>';
			echo '<div class="field">';
				echo $form->field($model, 'author_id')->dropDownList($authors)->label('Автор');
			echo '</div>';
		echo '</div>';
		echo '<div class="field">';
			echo $form->field($model, 'description')->textArea()->label('Описание');
		echo '</div>';
		?>
		<div class="form-actions">
			<?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'ui button primary')); ?>
		</div>

		<?php ActiveForm::end(); ?>
</div>