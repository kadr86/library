<div class="ui items">
  <div class="item">
    <a class="ui large image">
      <img src="<?php echo $model->preview ?>">
    </a>
    <div class="content">
      <a class="header"><?php echo $model->name ?></a>
      <div class="meta">
      	Автор: 
        <span class="stay"><?php echo $model->author->firstname ?> <?php echo $model->author->lastname ?></span>
      </div>
      <div class="description">
        <p><?php echo $model->description ?></p>
      </div>
    </div>
  </div>
</div>